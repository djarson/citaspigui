# Generated by Django 3.0.3 on 2023-07-26 05:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('scm', '0001_initial'),
        ('crm', '0003_remove_datemedical_ok_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='datemedical',
            name='provider',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='scm.Provider'),
            preserve_default=False,
        ),
    ]
