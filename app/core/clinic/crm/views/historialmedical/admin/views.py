import json
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import FormView
from core.clinic.crm.forms import *
from core.security.mixins import AccessModuleMixin


class HistorialMedicalAdminListView(AccessModuleMixin, FormView):
    form_class = CrmForm
    template_name = 'historialmedical/admin/list.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        action = request.POST.get('action', None)
        try:
            if action == 'search':
                data = []
                client = request.POST['client']
                search = DateMedical.objects.filter().order_by('date_joined')
                if len(client):
                    search = search.filter(client_id=client)
                for m in search:
                    data.append(m.toJSON())
            elif action == 'search_exams':
                data = []
                for i in DateMedicalExam.objects.filter(datemedical_id=request.POST['id']):
                    data.append(i.toJSON())
            elif action == 'search_medicines':
                data = []
                for i in DateMedicalProducts.objects.filter(datemedical_id=request.POST['id']):
                    data.append(i.toJSON())
            elif action == 'search_medicalparameter':
                data = []
                for i in DateMedicalParameters.objects.filter(datemedical_id=request.POST['id']):
                    data.append(i.toJSON())
            else:
                data['error'] = 'No ha seleccionado ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return HttpResponse(json.dumps(data), content_type='application/json')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Historiales Médicos'
        return context
